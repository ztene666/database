1. Membuat database:
	-create database myshop;
2. Membuat Table di dalam database:
	users
	-create table user(
		id int not null primary key,
		name varchar (255),
		email varchar(255),
		password varchar(255)
		);
	categories
	-create table categories(
		id int not null primary key,
		name varchar(255)
		);
	items
	-create table items(
		id int not null primary key,
		name varchar(255),
		description varchar(255),
		price int,
		stock int,
		category_id int,
		foreign key(category_id) references categories(id)
		);
3. Memasukkan Data pada table
	users
	-insert into users(id, name, email, password)
		values(1, "John Doe", "john@doe.com", "john123");
		values(2, "Jahn Doe", "jane@doe.com", "jenita123");
	categories
	-insert into categories(id, name)
		values(1, "gadget");
		values(2, "cloth");
		values(3, "men");
		values(4, "women");
		values(5, "branded");
	items
	-insert into items(id, name, description, price, stock, category_id)
		values(1, "Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1);
	-insert into items(id, name, description, price, stock, category_id)
		values(2, "Uniklooh", "baju keren dari brand ternama", 500000, 50, 2);
	-insert into items(id, name, description, price, stock, category_id)
		values(3, "IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);
4. Mengambil Data dari database
	a. mengambil data users
		-select name, email from users;
	b. mengambil data items
		-select *from items where price >1000000;
		-select *from items where name like "u%";
	c. menampilkan data items join dengan kategori
		-select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;
5. Mengubah data dari database
	update items set price = 2500000
		where id=1;